#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#ifdef NDEBUG

#define MEMFILE "/dev/mem"
#define TEMPFILE "/sys/class/thermal/thermal_zone0/temp"
#define CONFIGFILE "/etc/rpifanctl"

#define PWM0_BASE 0x7E20C000 // PWM0 register base (length 0x28)
#define GPIO_BASE 0x7E215000 // GPIO register base (length 0xF4)

#else

#define MEMFILE "./mem"
#define TEMPFILE "./temp"
#define CONFIGFILE "./rpifanctl"

#define GPIO_BASE 0x0000 // PWM0 register base (length 0x28)
#define PWM0_BASE 0x1000 // GPIO register base (length 0xF4)

#endif

#define GPFSEL1 0x04/4 // Function select for GPIO pins 10-19
#define GPIO12 0x800 // GPIO pin 12 for set/clr/lev operations
#define GPSET0 0x1C/4  // GPIO set for pins 0-31
#define GPCLR0 0x28/4  // GPIO clear for pins 0-31
#define DAT1 0x14/4    // Data for channel 1 of PWM0
#define CTL 0x00/4     // Controll register for PWM0


static uint32_t WAIT_INTERVAL = 1;
static int32_t MIN_TEMP = 50000;
static int32_t MAX_TEMP = 75000;

// https://elinux.org/RPi_GPIO_Code_Samples#Direct_register_access
int main()
{
	FILE *ftemp;
	FILE *fconfig;
	char atemp[8] = {};
	char aconfig[32] = {};
	int32_t temp;
	uint32_t pwm;
	int mem_fd;
	volatile uint32_t *gpio_map;
	volatile uint32_t *pwm_map;

	// Set up RPi4 GPIO mmap
	if ((mem_fd = open(MEMFILE, O_RDWR|O_SYNC) ) < 0) {
		fprintf(stderr, "Failed to open /dev/mem! Are you root?\n");
		return -1;
	}

	gpio_map = mmap(
		NULL,                 //Any adddress in our space will do
		0x1000,               //Map length
		PROT_READ|PROT_WRITE, // Enable reading & writting to mapped memory
		MAP_SHARED,           //Shared with other processes
		mem_fd,               //File to map
		GPIO_BASE             //Offset to GPIO peripheral
	);

	if (gpio_map == MAP_FAILED) {
		fprintf(stderr, "Failed to mmap GPIO! err %i: %s\n", errno, strerror(errno));
		return -1;
	}

	// Set up RPi4 PWM map
	pwm_map = mmap(
		NULL,
		0x1000,
		PROT_READ|PROT_WRITE,
		MAP_SHARED,
		mem_fd,
		PWM0_BASE
	);

	if (pwm_map == MAP_FAILED) {
		fprintf(stderr, "Failed to mmap PWM!\n");
		return -1;
	}

	close(mem_fd);

	do
	{
		// read the temperature
		ftemp = fopen(TEMPFILE,"r");
		if (ftemp != NULL)
		{
			fread(atemp, 8, 1,ftemp);
			fclose(ftemp);
		}
		else
		{
			fprintf(stderr, "Failed to read the temperature!\n");
			return -1;
		}

		// read the config file
		fconfig = fopen(CONFIGFILE, "r");
		if (fconfig != NULL)
		{
			fscanf(fconfig, "%i %i %i", &WAIT_INTERVAL, &MIN_TEMP, &MAX_TEMP);
			fclose(ftemp);
#ifndef NDEBUG
			printf("Config: interval %i s , min_t %i, max_t %i\n",
					WAIT_INTERVAL, MIN_TEMP, MAX_TEMP);
#endif
		}

		temp = atoi(atemp);
#ifndef NDEBUG
		printf("Temp: %i\n", temp);
#endif

		if (temp < MIN_TEMP)
		{
			// Turn off the fan
			*(pwm_map + CTL)  = (uint32_t)0x0;   // Disable PWM channel 1
			*(gpio_map + GPFSEL1) = (uint32_t)0x40;   // Set GPIO pin 12 to OUT
			*(gpio_map + GPCLR0)  = (uint32_t)GPIO12; // Set GPIO12 to High

		}
		else if (temp >= MIN_TEMP && temp < MAX_TEMP)
		{
			// PWM the fan
			pwm = (UINT32_MAX / (MAX_TEMP - MIN_TEMP)) * (temp - MIN_TEMP);
			*(pwm_map + CTL)  = (uint32_t)0x1;   // Enable PWM channel 1
			*(pwm_map + DAT1) = (uint32_t)pwm;   // PWM length
			*(gpio_map + GPFSEL1)  = (uint32_t)0x100; // Set GPIO pin 12 to ALT0 (PWM0_0)
		}
		else
		{
			// Turn the fan on full without PWM
			*(pwm_map + CTL)  = (uint32_t)0x0;   // Disable PWM channel 1
			*(gpio_map + GPFSEL1) = (uint32_t)0x40;  // Set GPIO pin 12 to OUT
			*(gpio_map + GPSET0)  = (uint32_t)GPIO12; // Set GPIO12 to High
		}

		sleep(WAIT_INTERVAL);
	} while (1);

	return 0;
}

