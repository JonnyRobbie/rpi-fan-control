Raspberry Pi 4 fan control
---

This utiliy aims to control a rpi fan connected to GPIO 12.

Config
---

Edit the `static const`s in the code and rebuild.

Build
---

`make`
ANd maybe install the systemd unit file.

Notes
---

* The fan should not be connected directly, but though a 1K0 resistor and a transistor.

* [The GPIO documentation](https://www.raspberrypi.org/documentation/hardware/raspberrypi/bcm2711/rpi_DATA_2711_1p0.pdf)

* [Wiring the fan](https://fizzy.cc/raspberry-pi-fan/)


