#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <pigpio.h>

#ifdef NDEBUG

#define TEMPFILE "/sys/class/thermal/thermal_zone0/temp"
#define CONFIGFILE "/etc/rpifanctl"

#else

#define TEMPFILE "./temp"
#define CONFIGFILE "./rpifanctl"

#endif

#define FAN_PIN 14

static uint32_t WAIT_INTERVAL = 1;
static int32_t MIN_TEMP = 45000;
static int32_t MAX_TEMP = 50000;

void interrupt(int sig)
{
#ifndef NDEBUG
	printf("Quitting fan control...\n");
#endif
	gpioTerminate();
	exit(sig);
}

int main()
{
	signal(SIGINT, interrupt);
	printf("Starting RPi fan control\n");
	FILE *ftemp;
	FILE *fconfig;
	char atemp[8] = {};
	char aconfig[32] = {};
	int32_t temp;
	uint32_t pwm;

	// initialize pigpio
	if (gpioInitialise() < 0)
	{
		fprintf(stderr, "Failed to initialize pigpio library!\n");
		return -2;
	}

	gpioSetMode(FAN_PIN, PI_OUTPUT);

	printf("Reading temperature from %s\n", TEMPFILE);
	printf("Reading config from %s\n", CONFIGFILE);
	do
	{
		// read the temperature
		ftemp = fopen(TEMPFILE,"r");
		if (ftemp != NULL)
		{
			fread(atemp, 8, 1,ftemp);
			fclose(ftemp);
		}
		else
		{
			fprintf(stderr, "Failed to read the temperature!\n");
			return -1;
		}

		// read the config file
		fconfig = fopen(CONFIGFILE, "r");
		if (fconfig != NULL)
		{
			fscanf(fconfig, "%i %i %i", &WAIT_INTERVAL, &MIN_TEMP, &MAX_TEMP);
			fclose(ftemp);
#ifndef NDEBUG
			printf("Config: interval %i s , min_t %i, max_t %i\n",
					WAIT_INTERVAL, MIN_TEMP, MAX_TEMP);
#endif
		}

		temp = atoi(atemp);
#ifndef NDEBUG
		printf("Temp: %i\n", temp);
#endif

		if (temp > MAX_TEMP)
		{
			//turn on
			gpioWrite(FAN_PIN, 1);
		}
		if (temp < MIN_TEMP)
		{
			//turn off
			gpioWrite(FAN_PIN, 0);
		}

	} while (!sleep(WAIT_INTERVAL));

	return 0;
}

