CC=gcc
CFLAGS=-I.
LIB=-lpigpio

release: fanpig.c
	$(CC) -o rpi-fan-control fanpig.c $(CFLAGS) -DNDEBUG $(LIB)

debug: fanpig.c
	$(CC) -o rpi-fan-control fanpig.c $(CFALGS) $(LIB)

clean:
	rm -f rpi-fan-control
	rm -f mem
	rm -f temp
	rm -f rpifanctl

